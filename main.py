from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
import pandas
import dash

app = dash.Dash(__name__, routes_pathname_prefix='/sss/')

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

dff = pandas.read_excel('newpj/new_masstransit_passenger_60_65.xlsx')
df = pandas.read_excel('newpj/new_masstransit_passenger_60_65.xlsx')
fd = pandas.read_excel('newpj/new_masstransit_passenger_60_65.xlsx')

dff.columns = [
    'month',
    'year',
    'project',
    'Total Pax',
    'Avg Pax for Day',
    'Avg Pax for OD',
    'Avg Pax for WD',
]
df.columns = [
    'month',
    'year',
    'project',
    'Total Pax',
    'Avg Pax for Day',
    'Avg Pax for OD',
    'Avg Pax for WD',
]
fd.columns = [
    'month',
    'year',
    'project',
    'Total Pax',
    'Avg Pax for Day',
    'Avg Pax for OD',
    'Avg Pax for WD',
]

df = df[~df['project'].str.contains('สายฉลองรัชธรรม')]

fd = fd[~fd['project'].str.contains('สายเฉลิมรัชมงคล')]

dff.replace({'มกราคม':'January', 'กุมภาพันธ์':'February',
            'มีนาคม':'March', 'เมษายน':'April',
            'พฤษภาคม':'May', 'มิถุนายน':'June',
            'กรกฎาคม':'July', 'สิงหาคม':'August',
            'กันยายน':'September', 'ตุลาคม':'October',
            'พฤศจิกายน':'November', 'ธันวาคม':'December',},
            inplace=True)


df.replace({'มกราคม':'January', 'กุมภาพันธ์':'February',
            'มีนาคม':'March', 'เมษายน':'April',
            'พฤษภาคม':'May', 'มิถุนายน':'June',
            'กรกฎาคม':'July', 'สิงหาคม':'August',
            'กันยายน':'September', 'ตุลาคม':'October',
            'พฤศจิกายน':'November', 'ธันวาคม':'December',},
            inplace=True)

fd.replace({'มกราคม':'January', 'กุมภาพันธ์':'February',
            'มีนาคม':'March', 'เมษายน':'April',
            'พฤษภาคม':'May', 'มิถุนายน':'June',
            'กรกฎาคม':'July', 'สิงหาคม':'August',
            'กันยายน':'September', 'ตุลาคม':'October',
            'พฤศจิกายน':'November', 'ธันวาคม':'December',},
            inplace=True)

months = df['month'].tolist()
months = list(set(months))

pio.templates.default = "plotly_dark"

# -------------------------------------------------------------------------------
# fig 1, 3
@app.callback(
    Output('bar-chart', 'figure'),
    Output('h-bar', 'figure'),
    Input('year-slider', 'value'))

# bar-chart, h-bar
def update_figure_1_3(selected_year1):
    filtered_df1 = df[df.year == selected_year1]    # h-bar 1
    filtered_df3 = df[df.year == selected_year1]    # bar-chart 3

    fig3 = px.bar(filtered_df3, x='month', 
                                y=['Avg Pax for OD', 'Avg Pax for WD'], 
                                color_discrete_sequence=['#b09bc7', '#5965a4'], 
                                barmode='group'
                                )
    fig3.update_layout(
        transition_duration=500,
        xaxis_title='Month',
        yaxis_title='Total passengers',
        font=dict(
            family="Arial",
            size=12,
            color="white"
        ),
        xaxis=dict(gridcolor='#283442'),
        yaxis=dict(gridcolor='#283442')
    )

    # h-bar
    fig1 = px.bar(filtered_df1, y='month',
                                x=['Total Pax' ,'Avg Pax for Day', 'Avg Pax for OD', 'Avg Pax for WD'], 
                                orientation='h', 
                                barmode='stack', 
                                color_discrete_sequence=['#5965a4', '#b09bc7', '#8ab1cf', '#3D61DD']
                            )
    fig1.update_layout(transition_duration=500,
                       xaxis_title='Passenger numbers',
                       yaxis_title='Month',
                       font=dict(
                            family="Arial",
                            size=12,
                            color="white"
                       )
                    )

    return fig3, fig1

# -------------------------------------------------------------------------------
# fig 2
@app.callback(
    Output('line-graph', 'figure'),
    Input('data-dropdown', 'value'),
    Input('year-slider', 'value'))

# line-graph
def update_figure_2(data_type, selected_year1):
    filtered_df = df[df.year == selected_year1]

    fig2 = px.line(filtered_df, x='month', y=data_type)
    fig2.update_traces(mode='markers+lines', hovertemplate=None, 
                    line_color='#A48EFF', marker_color='#2883FF',
                    marker=dict(size=10))
    fig2.update_layout(
                transition_duration=500,
                xaxis_title='Month',
                yaxis_title='Passenger numbers',
                font=dict(
                    family="Arial",
                    size=12,
                    color="white"
                ),
            )

    return fig2

def update_graph2(data_type, selected_year):
    fig = update_figure_2(selected_year, data_type)
    return fig
# -------------------------------------------------------------------------------
# fig 4
@app.callback(
    Output('passenger-plot', 'figure'),
    Input('month-dropdown', 'value'))

# pie-chart
def update_figure_4(selected_month):
    filtered_df = df[df['month'] == selected_month]
    
    fig4 = go.Figure()
    colors = ['#5965a4', '#CFA9F9', '#B177F1', '#3D61DD', '#8ab1cf', '#6B80EA']
    fig4.add_trace(go.Pie(
        labels=filtered_df['year'].unique(),
        values=filtered_df.groupby('year')['Total Pax'].sum(),
        title=f"{selected_month}", 
        hole = 0.35,
        marker=dict(colors=colors)
    ))
    fig4.update_layout(
        transition_duration=500,
        height=425,
        width=610,
        font=dict(
            family="Arial",
            size=12,
            color="white"
        ),
        margin=dict(t=50, r=50),
        title={
            'text': f'{selected_month}',
            'font': {'size': 20},
            'x': 0.5
        },
    )

    return fig4

def update_graph4(selected_month):
    fig = update_figure_4(selected_month)
    return fig
# -------------------------------------------------------------------------------

@app.callback(Output("bar-graph", "figure"),Input("year-slider", "value"))
def bar_show(selected_year): #ผู้โดยสารรวม
    filtered_df1 = fd[fd.year == selected_year]

    fig1 = px.bar(
        filtered_df1,
        y ="Total Pax",
        x = "month",
        color="month",
        template="plotly_dark"

    )
    return fig1
#---------------------------------------------------------------------------------

@app.callback(Output("pie-graph", "figure"), Input("year-slider", "value"))
def pie_show(selected_year): #ผู้โดยสารเฉลี่ยรายวัน 
    filtered_df2 = fd[fd.year == selected_year]
    fig2 = px.pie(
        filtered_df2,
        values="Avg Pax for Day",
        names="month",
        color="month",
        hole= 0.4,template="plotly_dark"
    )
    return fig2
#---------------------------------------------------------------------------------

@app.callback(Output("barh1-graph", "figure"), Input("year-slider", "value"))
def barh1_show(selected_year): #ผู้โดยสารเฉลี่ยวันธรรมดา
    filtered_df3 = fd[fd.year == selected_year]
    fig3 = px.bar(
        filtered_df3,
        x="Avg Pax for OD",
        y = "month",
        color="month",
        orientation='h',
        template="plotly_dark"
    )
    
    return fig3

#---------------------------------------------------------------------------------

@app.callback(Output("barh2-graph", "figure"), Input("year-slider", "value"))
def barh2_show(selected_year): #ผู้โดยสารเฉลี่ยวันหยุด
    filtered_df4 = fd[fd.year == selected_year]
    fig4 = px.bar(
        filtered_df4,
        x="Avg Pax for WD",
        y = "month",
        color="month",
        orientation='h',
        template="plotly_dark"
    )
    
    return fig4

#---------------------------------------------------------------------------------

@app.callback(Output("line-graph-2", "figure"), Input("year-slider", "value"))
def line_show(selected_year): #ผู้โดยสารเฉลี่ยวันหยุด
    filtered_df5 = fd[fd.year == selected_year]
    fig5 = px.line(
        filtered_df5,
        y = ["Avg Pax for OD","Avg Pax for WD"],
        x = "month",markers=True,
        template="plotly_dark"
        )
    fig5.update_layout(
    plot_bgcolor="white")

    return fig5
#---------------------------------------------------------------------------------
@app.callback(
    Output('passenger-plot-2', 'figure'),
    Input('month-dropdown-2', 'value'))

def update_figure_4(selected_month):

    filtered_df = df[df['month'] == selected_month]
    filtered_df_2 = fd[fd['month'] == selected_month]

    fig = go.Figure()
    
    for year in filtered_df['year'].unique():
        data_year = filtered_df[filtered_df['year'] == year]
        fig.add_trace(go.Bar(x=data_year['year'], y=data_year['Total Pax'], name=str(year)))
    
    for year in filtered_df_2['year'].unique():
        data_year = filtered_df_2[filtered_df_2['year'] == year]
        fig.add_trace(go.Bar(x=data_year['year'], y=data_year['Total Pax'], name=str(year)))
        
    fig.update_layout(
        title=f'Total passengers for {selected_month}',
        xaxis_title='Year',
        yaxis_title='Total passengers',
        barmode='group',
        colorway=['#a256d5', '#d55677', '#85C1E9'] 
    )

    return fig

def update_graph(selected_month):
    fig = update_figure_4(selected_month)
    return fig



app.layout = dbc.Container(
    [   # ROW 1 ------------------------------------------------------------------------------------------------------------
        dbc.Row(
            [   # ข้อมูลโครงการรถไฟฟ้ามหานคร สายเฉลิมรัชมงคล
                html.H4("Mass Rapid Transit Project Information Chaloem Ratchamongkhon Line", 
                        style={'textAlign': 'center', 'margin-top': '10px', 'font-family': 'Georgia'}),
            ]
        ),
        # ROW 2 ------------------------------------------------------------------------------------------------------------
        dbc.Row(
            [
                dbc.Col(
                    dcc.Slider(
                        df['year'].min(),
                        df['year'].max(),
                        step=None,
                        value=df['year'].min(),
                        marks={str(year): str(year) for year in df['year'].unique()},
                        id='year-slider'
                    ),
                    width={'size': 10, 'offset': 1},
                    style={'margin-top': '10px', 'font-family': 'Times New Roman'}
                ),
            ]
        ),
        # ROW 3 ------------------------------------------------------------------------------------------------------------
        dbc.Row(
            [   
                dbc.Col(
                    dbc.Card(
                        [   # แสดงจำนวนผู้โดยสารรวม, เฉลี่ยน รายวัน, วันธรรมดา, วันหยุด เปรียบเทียบกัน - h-bar
                            dbc.CardHeader('Avg. daily, Weekday, and Weekend passenger numbers', 
                                           style={'textAlign': 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}
                                            ), 
                            dcc.Graph(id='h-bar', style={'border-radius': '10px', 
                                                         'overflow': 'hidden'}
                                                    ),
                        ],
                        style={'height': '400px',  
                               'borderRadius': '10px', 
                               'background-color': '#111111', 
                               'border-color': '#5855FF', 
                               'border-width': '1px', 
                               'border-style': 'solid'
                               },
                    ),
                    width={'size': 6},
                ),
                dbc.Col(
                    dbc.Card(
                        [   # แสดงจำนวนผู้โดยสารรวม, เฉลี่ยน รายวัน, วันธรรมดา, วันหยุด - line-graph
                            dbc.CardHeader('Avg. Daily, Weekdays, Weekend and Total passenger numbers', 
                                           style={'textAlign' : 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}
                                            ),
                            dcc.Dropdown(
                                id='data-dropdown',
                                options=[{'label': 'Total Pax', 'value': 'Total Pax'},
                                        {'label': 'Avg Pax for Day', 'value': 'Avg Pax for Day'},
                                        {'label': 'Avg Pax for OD', 'value': 'Avg Pax for OD'},
                                        {'label': 'Avg Pax for WD', 'value': 'Avg Pax for WD'}
                                        ],
                                value='Total Pax',
                                style={'background-color': '#111111', 
                                       'color': '#111111',
                                       'border-color': '#5A58FF'
                                       }
                            ),
                            dcc.Graph(id='line-graph', style={'border-radius': '10px', 
                                                              'overflow': 'hidden'}
                                                            ),
                        ],
                        style={'height': '400px', 
                               'borderRadius': '10px', 
                               'background-color': '#111111', 
                               'border-color': '#5855FF', 
                               'border-width': '1px', 
                               'border-style': 'solid'
                               },
                    ),
                    width={'size': 6}
                )
            ]
        ),
        # ROW 4 ------------------------------------------------------------------------------------------------------------
        dbc.Row(
            [ 
                dbc.Col(
                    dbc.Card(
                        [   # เปรียบเทียบจำนวนผู้โดยสารในวันธรรมดาและวันหยุดสุดสัปดาห์ - bar-chart
                            dbc.CardHeader('Weekday vs. Weekend passenger numbers', 
                                           style={'textAlign' : 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}
                                                ),
                            dcc.Graph(id='bar-chart', style={'border-radius': '10px', 
                                                             'overflow': 'hidden'}
                                                        ),
                        ],
                        style={'height': '450px', 
                               'borderRadius': '10px', 
                               'background-color': '#111111', 
                               'border-color': '#5855FF', 
                               'border-width': '1px', 
                               'border-style': 'solid'
                               },
                    ),
                    width={'size': 7},
                ),
                dbc.Col(
                    dbc.Card(
                        [   # แสดงจำนวนผู้โดยสารรวมในเดือนเดียวกันในแต่ละปี - pie-graph
                            dbc.CardHeader('Total Monthly passengers by Year', 
                                           style={'textAlign' : 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}
                                                ),
                            dcc.Dropdown(
                                id='month-dropdown',
                                options=[{'label': month, 'value': month} for month in df['month'].unique()],
                                value=df['month'].unique()[0],
                                style={'background-color': '#111111', 
                                       'color': '#111111',
                                       'border-color': '#5A58FF'}
                            ),
                            dcc.Graph(id='passenger-plot', 
                                      style={'border-radius': '10px', 
                                             'overflow': 'hidden'}, 
                                             figure=update_graph4(df['month'].unique()[0])
                                        ),
                        ],
                        style={'height': '450px', 
                               'borderRadius': '10px', 
                               'background-color': '#111111', 
                               'fontsize' : '30px', 
                               'border-color': '#5855FF', 
                               'border-width': '1px', 
                               'border-style': 'solid'
                               },
                    ),
                    width={'size': 5},
                ),
            ],
            style={'margin-top': '10px', 
                   'align':'center'}
        ),
        dbc.Row(
            [   # ข้อมูลโครงการรถไฟฟ้ามหานคร สายฉลองรัชธรรม
                html.H4("Mass Rapid Transit Project Information  Chalong Ratchadham Line", 
                        style={"textAlign": "center",'font-family':'Georgia','color':'white','shadow':'True'}),
            ],
            style={'margin-top': '50px', 'align':'center'}
        ),
        dbc.Row(
            [dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('TOTAL PASSENGERS', style={'textAlign' : 'center', 'color':'yellow','font-family':'Georgia',}),
                        dcc.Graph(id="bar-graph"),
                        
                    ],style={'height': '400px', 'borderRadius': '10px',},
                    # className="col-6",
                ),width={'size': 12},
               ),
            ],style={'margin-top': '70px', 'align':'center'}
        ),
        dbc.Row(
            [ 
                dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS NORMDAY', style={'textAlign' : 'center', 'color':'red','font-family':'Georgia',}),
                        dcc.Graph(id="barh1-graph"),
                        #  dcc.Graph(id="barh2-graph"),
                    ],style={'height': '400px', 'borderRadius': '0px'},
        #),
                    # className="col-6",
                ),width={'size': 5},
                ),
                dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS WEEKEND', style={'textAlign' : 'center', 'color':'lightgreen','font-family':'Georgia',}),
                        dcc.Graph(id="barh2-graph"),
                    ],style={'height': '400px', 'borderRadius': '0px'},
                    # className="col-6",
                ),width={'size': 5},
                ),
            ],
            style={'margin-top': '130px','margin-left': '200px','align':'center'}
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        [   dbc.CardHeader('SUM PASSENGERS DAY', style={'textAlign' : 'center', 'color':'pink','font-family':'Georgia',}),
                            dcc.Graph(id="pie-graph"),
                        ],style={'height': '175px', 'borderRadius': '10px'},
                    ),width={'size': 5},
                ),
                dbc.Col(
                    dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS NORMDAY VS WEEKEND', style={'textAlign' : 'center', 'color':'white','font-family':'Georgia',}),
                        dcc.Graph(id="line-graph-2"),
                    ],style={'height': '500px', 'borderRadius': '10px'}
                    # className="col-6",
                ),width={'size': 7},
                ),
            ],style={'margin-top': '150px', 'align':'center'}
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        [   
                            dbc.CardHeader('Graph showing monthly passenger comparison of both stations', 
                                           style={'textAlign' : 'center', 
                                                  'font-family': 'Georgia', 
                                                  'color': '#D2D5FF', 
                                                  'font-size' : '20px'}),
                            dcc.Dropdown(
                                id='month-dropdown-2',
                                options=[{'label': month, 'value': month} for month in df['month'].unique()],
                                value=df['month'].unique()[0],
                                style={'width': '730px',
                                        'height': '40px',
                                        'border-radius': '0px',
                                    },
                            ),
                            dcc.Graph(
                                id='passenger-plot-2', 
                                style={ 'height': '450px', 
                                       'margin-top':'0px',
                                        'border-radius': '5px'},
                            ),
                        ],
                        style={'margin-top':'60px'}
                    
                    ),
                    width=12,
                    style={
                        'margin-top': '10px', 
                        'align':'center',
                    }
                ),
            ]
        ),
    ],
    fluid=True,
    style = {'background-image': 'url("https://tinypic.host/images/2023/02/18/Presentation16b919a2b78ccd144.png")'}
)

if __name__ == '__main__':
    app.run_server(debug=True)