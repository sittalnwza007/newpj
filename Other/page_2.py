from dash import Dash, dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import pandas

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])

style={ 'backgroundColor': '#E7DCFC'}




# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
df = pandas.read_excel("newpj/new_masstransit_passenger_60_65.xlsx")
df.columns = [
    "month",
    "year",
    "project",
    "total_passengers",
    "sum_passengers_day",
    "avg_passengers_normday",
    "avg_passengers_weekend",

]
df = df[~df['project'].str.contains('สายเฉลิมรัชมงคล')]

df.replace({"มกราคม":"January", "กุมภาพันธ์":"February",
            "มีนาคม":"March", "เมษายน":"April",
            "พฤษภาคม":"May", "มิถุนายน":"June",
            "กรกฎาคม":"July", "สิงหาคม":"August",
            "กันยายน":"September", "ตุลาคม":"October",
            "พฤศจิกายน":"November", "ธันวาคม":"December",},
            inplace=True)




@app.callback(Output("bar-graph", "figure"),Input("year-slider", "value"))
def bar_show(selected_year): #ผู้โดยสารรวม
    filtered_df1 = df[df.year == selected_year]

    fig1 = px.bar(
        filtered_df1,
        y ="total_passengers",
        x = "month",
        color="month",
        template="plotly_dark"

    )
    

    return fig1 
@app.callback(Output("pie-graph", "figure"), Input("year-slider", "value"))
def pie_show(selected_year): #ผู้โดยสารเฉลี่ยรายวัน 
    filtered_df2 = df[df.year == selected_year]
    fig2 = px.pie(
        filtered_df2,
        values="sum_passengers_day",
        names="month",
        color="month",
        hole= 0.4,template="plotly_dark"
    
        
    )
    return fig2

@app.callback(Output("barh1-graph", "figure"), Input("year-slider", "value"))
def barh1_show(selected_year): #ผู้โดยสารเฉลี่ยวันธรรมดา
    filtered_df3 = df[df.year == selected_year]
    fig3 = px.bar(
        filtered_df3,
        x="avg_passengers_normday",
        y = "month",
        color="month",
        orientation='h',
        template="plotly_dark"
    )
    
    return fig3

@app.callback(Output("barh2-graph", "figure"), Input("year-slider", "value"))
def barh2_show(selected_year): #ผู้โดยสารเฉลี่ยวันหยุด
    filtered_df4 = df[df.year == selected_year]
    fig4 = px.bar(
        filtered_df4,
        x="avg_passengers_weekend",
        y = "month",
        color="month",
        orientation='h',
        template="plotly_dark"
    )
    
    return fig4
# power = df[df]
@app.callback(Output("line-graph", "figure"), Input("year-slider", "value"))
def line_show(selected_year): #ผู้โดยสารเฉลี่ยวันหยุด
    filtered_df5 = df[df.year == selected_year]
    fig5 = px.line(
        filtered_df5,
        y = ["avg_passengers_normday","avg_passengers_weekend"],
        # y2 ="avg_passengers_weekend",
        x = "month",markers=True,
        template="plotly_dark"
        
        )
    fig5.update_layout(
    plot_bgcolor="white",
    # margin=dict(t=10,l=10,b=10,r=10)
)

    
    return fig5
# app._background_manager ([
#     html.c

# ]

# )

app.layout = dbc.Container(
    [ 
        dbc.Row(
            [
                html.H1(
                    "Mass Rapid Transit Project Information  Chalong Ratchadham Line",
                    style={
                        "textAlign": "center",'font-family':'Georgia','color':'white','shadow':'True',
                    },
                )
            ],style={'margin-top': '70px', 'align':'center'},
            className="row",
        ),
        dbc.Row(
            [
                dbc.Col(
                    [
                        dcc.Slider(
                            df["year"].min(),
                            df["year"].max(),
                            step=None,
                            value=df["year"].min(),
                            marks={str(year): str(year) for year in df["year"].unique()},
                            id="year-slider",
                        ),
                    ],style={
                        "textAlign": "center",'font-family':'Georgia','color':'gold',}
                    
                    # className="col-sm",
                ),
            ],style={"padding-top": "40px;","padding-left":"70px"},
            
        ),
        
        dbc.Row(
            [dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('TOTAL PASSENGERS', style={'textAlign' : 'center', 'color':'yellow','font-family':'Georgia',}),
                        dcc.Graph(id="bar-graph"),
                        
                    ],style={'height': '400px', 'borderRadius': '10px',},
                    # className="col-6",
                ),width={'size': 12},
               ),
            ],style={'margin-top': '70px', 'align':'center'}
        ),
        dbc.Row(
            [
                dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS NORMDAY', style={'textAlign' : 'center', 'color':'red','font-family':'Georgia',}),
                        dcc.Graph(id="barh1-graph"),
                        #  dcc.Graph(id="barh2-graph"),
                    ],style={'height': '400px', 'borderRadius': '0px'},
        # ),
                    # className="col-6",
                ),width={'size': 5},
                ),
                dbc.Col(
                dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS WEEKEND', style={'textAlign' : 'center', 'color':'lightgreen','font-family':'Georgia',}),
                        dcc.Graph(id="barh2-graph"),
                    ],style={'height': '400px', 'borderRadius': '0px'},
                    # className="col-6",
                ),width={'size': 5},
                ),
            ],style={'margin-top': '130px','margin-left': '200px','align':'center'}
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        [   dbc.CardHeader('SUM PASSENGERS DAY', style={'textAlign' : 'center', 'color':'pink','font-family':'Georgia',}),
                            dcc.Graph(id="pie-graph"),
                        ],style={'height': '175px', 'borderRadius': '10px'},
                    ),width={'size': 5},
                ),
                dbc.Col(
                    dbc.Card(
                    [dbc.CardHeader('AVG PASSENGERS NORMDAY VS WEEKEND', style={'textAlign' : 'center', 'color':'white','font-family':'Georgia',}),
                        dcc.Graph(id="line-graph"),
                    ],style={'height': '500px', 'borderRadius': '10px'}
                    # className="col-6",
                ),width={'size': 7},
                ),
            ],style={'margin-top': '150px', 'align':'center'}
        ),
                
    ],style={ 'background-image':'url("https://png.pngtree.com/background/20210714/original/pngtree-blue-space-light-universe-starry-planet-background-picture-image_1196382.jpg")','backdrop-filter': 'blur(5px)'}
)


if __name__ == "__main__":
    app.run_server(debug=True)
