from dash import Dash, dcc, html, Input, Output 
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import plotly.express as px
import pandas
import dash

external_stylesheets = ['assets/style.css']

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])
app = dash.Dash(__name__, external_stylesheets=['assets/style.css'])


df = pandas.read_excel('newpj/new_masstransit_passenger_60_65.xlsx')
df_2 = pandas.read_excel('newpj/new_masstransit_passenger_60_65.xlsx')

df.columns = [
    'month',
    'year',
    'project',
    'Total Pax',
    'Avg Pax for Day',
    'Avg Pax for OD',
    'Avg Pax for WD',
]
df_2.columns = [
    'month',
    'year',
    'project',
    'Total Pax',
    'Avg Pax for Day',
    'Avg Pax for OD',
    'Avg Pax for WD',
]

df = df[~df['project'].str.contains('สายฉลองรัชธรรม')]
df_2 = df_2[~df_2['project'].str.contains('สายเฉลิมรัชมงคล')]

df.replace({'มกราคม':'January', 'กุมภาพันธ์':'February',
            'มีนาคม':'March', 'เมษายน':'April',
            'พฤษภาคม':'May', 'มิถุนายน':'June',
            'กรกฎาคม':'July', 'สิงหาคม':'August',
            'กันยายน':'September', 'ตุลาคม':'October',
            'พฤศจิกายน':'November', 'ธันวาคม':'December',},
            inplace=True)

df_2.replace({'มกราคม':'January', 'กุมภาพันธ์':'February',
            'มีนาคม':'March', 'เมษายน':'April',
            'พฤษภาคม':'May', 'มิถุนายน':'June',
            'กรกฎาคม':'July', 'สิงหาคม':'August',
            'กันยายน':'September', 'ตุลาคม':'October',
            'พฤศจิกายน':'November', 'ธันวาคม':'December',},
            inplace=True)

months = df['month'].tolist()
months = list(set(months))
months_2 = df_2['month'].tolist()
months_2 = list(set(months_2))


@app.callback(
    Output('line-graph', 'figure'),
    Output('line-graph_2', 'figure'),
    Output('bar-chart', 'figure'),
    Output('bar-chart_2', 'figure'),
    Output('h-bar', 'figure'),
    Output('h-bar_2', 'figure'),

    Input('year-slider', 'value'))

# line-graph, bar-chart, h-bar
def update_figure_1_2_3(selected_year1):
    filtered_df1 = df[df.year == selected_year1]
    filtered_df_2 = df_2[df_2.year == selected_year1]    # line-graph

    
    filtered_df2 = df[df.year == selected_year1]    # bar-chart
    filtered_df2_2 = df_2[df_2.year == selected_year1]    # bar-chart

    filtered_df3 = df[df.year == selected_year1]    # h-bar
    filtered_df3_2 = df_2[df_2.year == selected_year1]    # h-bar


    # line-graph
    fig1 = px.line(filtered_df1, x='month', y=['Total Pax'])
    fig1.update_traces(mode='markers+lines', hovertemplate=None, line_color='#FF0000')
    fig1.update_layout(transition_duration=500,
                       xaxis_title='Month',
                       yaxis_title=' Passenger numbers(สายฉลองราชธรรม)'
                    )
    
    fig1_2 = px.line(filtered_df_2, x='month', y=['Total Pax'])
    fig1_2.update_traces(mode='markers+lines', hovertemplate=None, line_color='#ff9900')
    fig1_2.update_layout(transition_duration=500,
                       xaxis_title='Month',
                       yaxis_title='Passenger numbers(สายเฉลิมรัชมงคล)'
                    )

    # bar-chart
    fig2 = px.bar(filtered_df2, x='month', y=['Avg Pax for OD', 'Avg Pax for WD'],  barmode='group',color_discrete_sequence=['#34a679', '#65c9d2'])
    fig2.update_layout(transition_duration=500,
                       xaxis_title='Month',
                       yaxis_title='Total passengers'
                    )
    fig2_2 = px.bar(filtered_df2_2, x='month', y=['Avg Pax for OD', 'Avg Pax for WD'],  barmode='group')
    fig2_2.update_layout(transition_duration=500,
                       xaxis_title='Montsh',
                       yaxis_title='Total passengers'
                    )
    fig3 = px.bar(filtered_df3, x=['Avg Pax for Day', 'Avg Pax for OD', 'Avg Pax for WD'], y='month', orientation='h', barmode='stack', color_discrete_sequence=px.colors.sequential.Greens)
    fig3.update_layout(transition_duration=500,
                       xaxis_title='Passenger numbers',
                       yaxis_title='Month'   
                    )
    fig3_2 = px.bar(filtered_df3_2, x=['Avg Pax for Day', 'Avg Pax for OD', 'Avg Pax for WD'], y='month', orientation='h', barmode='stack', color_discrete_sequence=px.colors.sequential.Reds)
    fig3_2.update_layout(transition_duration=500,
                       xaxis_title='Passenger numbers',
                       yaxis_title='Month'   
                    )


    return fig1,fig1_2,fig2,fig2_2,fig3,fig3_2

# bar-chart
def update_figure_4(selected_month):

    filtered_df = df[df['month'] == selected_month]
    filtered_df_2 = df_2[df_2['month'] == selected_month]

    fig = go.Figure()
    
    for year in filtered_df['year'].unique():
        data_year = filtered_df[filtered_df['year'] == year]
        fig.add_trace(go.Bar(x=data_year['year'], y=data_year['Total Pax'], name=str(year)))
    
    for year in filtered_df_2['year'].unique():
        data_year = filtered_df_2[filtered_df_2['year'] == year]
        fig.add_trace(go.Bar(x=data_year['year'], y=data_year['Total Pax'], name=str(year)))
        
    fig.update_layout(
        title=f'Total passengers for {selected_month}',
        xaxis_title='Year',
        yaxis_title='Total passengers',
        barmode='group',
        colorway=['#a256d5', '#d55677', '#85C1E9'] 
    )

    return fig

@app.callback(
    Output('passenger-plot', 'figure'),
    Input('month-dropdown', 'value'))
def update_graph(selected_month):
    fig = update_figure_4(selected_month)
    return fig

app.layout = dbc.Container(
    [   
        dbc.Row(
            [
                dbc.Col(dcc.Slider(
        df['year'].min(),
        df['year'].max(),
        step=None,
        value=df['year'].min(),
        marks={str(year): str(year) for year in df['year'].unique()},
        id='year-slider',
        className='slider'
    ),
    width=10,
    style={'margin-top': '10px', 'offset': 1}
)
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Card(
                        [   
                            dbc.CardHeader('Total passengers : สายฉลองรัชธรรม', style={'textAlign': 'center', 'font-size': '18px','padding-top':'5px', 'color': '#FFFFFF'}),
                            dcc.Graph(id='line-graph', style={'border-radius': '10px', 'overflow': 'hidden'}),
                        ],
                        style={'margin-top':'60px','height': '500px', 'borderRadius': '10px', 'background-color': '#f6f6f6'},
                    ),
                    width=6,
                ), 
                dbc.Col(
                    dbc.Card(
                        [   
                            dbc.CardHeader('Total passengers : สายเฉลิมรัชมงคล', style={'textAlign': 'center', 'font-size': '18px', 'padding-top':'5px','color': '#f6f6f6'}),
                            dcc.Graph(id='line-graph_2', style={'border-radius': '10px', 'overflow': 'hidden'}),
                        ],
                        style={'margin-top':'60px','height': '500px', 'borderRadius': '10px', 'background-color': '#f6f6f6'},
                    ),
                    width=6,
                ),
                dbc.Col(
                    dbc.Card(
                        [   
                            dbc.CardHeader('Passenger Numbers : สายฉลองรัชธรรม', style={'textAlign': 'center', 'font-size': '18px', 'padding-top':'5px','color': '#FFFFFF'}),
                            dcc.Graph(id='bar-chart', style={'border-radius': '10px', 'overflow': 'hidden'}),
                        ],
                        style={'margin-top':'60px','height': '450px', 'borderRadius': '10px', 'background-color': '#f6f6f6'},
                    ),
                    width=6,
                ),
                dbc.Col(
                    dbc.Card(
                        [   
                            dbc.CardHeader('Passenger Numbers : สายเฉลิมรัชมงคล', style={'textAlign': 'center', 'font-size': '18px','padding-top':'5px', 'color': '#FFFFFF'}),
                            dcc.Graph(id='bar-chart_2', style={'border-radius': '10px', 'overflow': 'hidden'}),
                        ],
                        style={'margin-top':'60px','height': '450px', 'borderRadius': '10px', 'background-color': '#f6f6f6'},
                    ),
                    width=6,
                ),
            ],
            style={'margin-top': '10px', 'align':'center'}
        ),
       
        dbc.Col(
            dbc.Card(
                [   
                    dbc.CardHeader('Avg. daily, weekday, and holiday passenger numbers: สายฉลองรัชธรรม',id='n-card', style={'textAlign': 'center','margin-top':'50px', 'padding-top':'5px','font-size': '18px', 'color': '#FFFFFF'}), 
                    dcc.Graph(
                        id='h-bar', 
                        style={
                            'border-radius': '10px', 
                            'overflow': 'hidden', 
                            'height': '100%',
                            'background-color': '#2C3E50',
                            'padding': '20px',
                            'box-shadow': '5px 5px 10px #888888'
                        }
                    ),
                ],
                style={
                    'height': '450px',
                    'margin-top':'60px', 
                    'borderRadius': '10px', 
                    'background-color': '#FFFFFF',
                    'box-shadow': '5px 5px 10px #888888'
                },
            ),
            width=6,
        ),
        dbc.Col(
            dbc.Card(
                [  
     
                    dbc.CardHeader('Avg. daily, weekday, and holiday passenger numbers: สายเฉลิมรัชมงคล  ',className='cardHeadN', style={'textAlign': 'center', 'padding-top':'5px','font-size': '18px', 'color': '#FFFFFF','margin-top':'50px'}), 
                    dcc.Graph(
                        id='h-bar_2', 
                        style={
                            'border-radius': '10px', 
                            'overflow': 'hidden', 
                            'height': '100%',
                            'background-color': '#2C3E50',
                            'padding': '20px',
                            'box-shadow': '5px 5px 10px #888888'
                        }
                    ),
                ],
                style={
                    'margin-top':'200px',
                    'height': '450px', 
                    'borderRadius': '10px', 
                    'background-color': '#FFFFFF',
                    'box-shadow': '5px 5px 10px #888888'
                },
            ),
            width=6,
        ),
        dbc.Col(
            dbc.Card(
                [   
                    dbc.CardHeader('Graph showing monthly passenger comparison of both stations', style={'width':'600px','margin-left':'40px','padding-top':'5px','textAlign': 'center','color':'#f6f6f6', 'font-size': '18px','margin-top':'200px','background-color':'#884A98'}), 
                    dcc.Dropdown(
                        id='month-dropdown',
                        options=[{'label': month, 'value': month} for month in df['month'].unique()],
                        value=df['month'].unique()[0],
                        style={'width': '400px',
            'height': '40px',
            'margin': '20px 23px 20px 20px',
            'border-radius': '5px',
            'border': '1px solid #ddd',
                               },
                    ),
                    dcc.Graph(
                        id='passenger-plot', 
                       style={  'height': '450px', 
            'border': '1px solid #ddd', 
            'border-radius': '5px'},

                    ),
                ],
               
            ),
            width=12,
        

    style={
        'margin-top': '10px', 
        'align':'center',
        'box-shadow': '5px 5px 10px #888888'
    }
),

    ],
    style={'height': '1400px'}
    ,fluid=True,
)

if __name__ == '__main__':
    app.run_server(debug=True)